module gitlab.com/bartossh/lango_quick

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0
)
